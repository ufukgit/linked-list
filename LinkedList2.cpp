#include <stdio.h>
#include <stdlib.h>

typedef struct n {
	int data;
	n * next;
}node;

void printLinkedList(node* value)
{
	while (value->next != NULL)
	{
		printf("Linked List Value = %d \n", value->data);
		value = value->next;
	}
}

void addValueToList(node* root, int value)
{
	while (root->next!=NULL)
	{
		root = root->next;
	}
	root->next = (node*)malloc(sizeof(node));
	root->next->data = value;
	printf("Add Value : %d \n", value);
	root->next->next = NULL;
}

int main()
{
	node * root;
	root = (node*)malloc(sizeof(node));
	root->next = NULL;
	root->data = 500;
	for (int i = 0; i < 5; i++)
	{
		addValueToList(root, i*10);
	}
	printLinkedList(root);
	
	node * iter = root;
	for (int i=0; i<2; i++)
	{
		iter = iter->next;
	}
	node * temp = (node*)malloc(sizeof(node));
	temp->next = iter->next;
	iter->next = temp;
	temp->data = 85;
	printLinkedList(root);	
}
