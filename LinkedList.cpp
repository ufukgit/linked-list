#include <stdio.h>
#include <stdlib.h>

typedef struct n {
	int data;
	n * next;
}node;

void printLinkedList(node* value)
{
	while (value->next != NULL)
	{
		printf("Linked List Value = %d \n", value->data);
		value = value->next;
	}
}
int main()
{
	node * root;
	root = (node*)malloc(sizeof(node));
	root->data = 10;
	root->next = (node*)malloc(sizeof(node));
	root->next->data = 20;
	root->next->next = (node*)malloc(sizeof(node));
	root->next->next->data = 30;
	root->next->next->next = NULL;
	node * iter;
	iter = root;
	printf("%d\n", iter->data);
	iter = iter->next;
	printf("%d\n", iter->data);
	iter = root;
	int i = 0;
	while (iter->next != NULL)
	{
		i++;
		printf("%d.inci eleman : %d\n", i, iter->data);
		iter = iter->next;
	}
	for (int i = 0; i < 5; i++)
	{
		iter->next = (node*)malloc(sizeof(node));
		iter = iter->next;
		iter->data = i * 10;
		iter->next = NULL;
	}
	printLinkedList(root);
}
