#include <stdio.h>
#include <stdlib.h>
typedef struct n{
 int x;
 struct n * next;
} node;

void printToLinkList(node *r){
 while(r!=NULL){
  printf("%d ",r->x);
  r=r->next;
 }
}

void addValue(node *r, int x){
 while(r->next!=NULL){
  r = r -> next;
 }
 r->next = (node*)malloc(sizeof(node));
 r->next->x = x;
 r->next->next = NULL;
}

node * ekleSirali(node * r, float x){
 if(r==NULL){//linklist bossa
  r=(node*)malloc(sizeof(node));
  r->next= NULL;
  r->x = x;
  return r;
 }
 if(r->x > x){ // ilk elemandan kucuk durumu
 // r degisiyor
  node * temp = (node*)malloc(sizeof(node));
  temp -> x = x;
  temp -> next = r;
  return temp;
 }
 node * iter = r;
 while (iter -> next != NULL && iter ->next-> x < x){
  iter= iter -> next;
 }
 node * temp = (node*)malloc(sizeof(node));
 temp -> next = iter -> next;
 iter -> next = temp;
 temp -> x = x;
 return r;
}

int main(){
 node * root;
 root = NULL;
 root = ekleSirali(root,75);
 root = ekleSirali(root,129);
 root = ekleSirali(root,157);
 root = ekleSirali(root,3);
 root = ekleSirali(root,57);
 root = ekleSirali(root,29);
 printToLinkList(root);
}
